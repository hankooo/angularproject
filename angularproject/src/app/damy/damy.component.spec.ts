import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DamyComponent } from './damy.component';

describe('DamyComponent', () => {
  let component: DamyComponent;
  let fixture: ComponentFixture<DamyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DamyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DamyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HoussemComponent } from './houssem.component';

describe('HoussemComponent', () => {
  let component: HoussemComponent;
  let fixture: ComponentFixture<HoussemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HoussemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HoussemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-user-line',
  templateUrl: './user-line.component.html',
  styleUrls: ['./user-line.component.css']
})
export class UserLineComponent implements OnInit {
  @Input() userData;
  @Output() giveName = new EventEmitter();
  constructor() { }



  ngOnInit() {
  }
  clickPressed() {
    console.log(this.userData)
    this.giveName.emit(this.userData)
  }
}

import { Component, OnInit } from '@angular/core';
import { AppComponent } from './../app.component';
import { UsersService } from './../services/users/users.service';

@Component({
  selector: 'app-houssem',
  templateUrl: './houssem.component.html',
  styleUrls: ['./houssem.component.css']
})
export class HoussemComponent implements OnInit {
  title: string = 'Houssem'
  users = [];
  outPutName;
  constructor(private _usersService: UsersService
  ) { }

  ngOnInit() {

    this._usersService.getUsers().subscribe(usersData => this.users = usersData);
    console.log(this.users)
  };

  parentRecieveEvent(whatever) {
    console.log("parentRecieveEvant")
    console.log(whatever)
    this.outPutName = whatever;
  }
}



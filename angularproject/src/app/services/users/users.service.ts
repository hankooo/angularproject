import { Injectable } from '@angular/core';
import { of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class UsersService {
  users: Array<object> = [
    { name: 'Conor McGregor', age: 21, unicorns: 3, tel: 325689725 },
    { name: 'Tony Ferguson', age: 23, unicorns: 3, tel: 123489725 },
    { name: 'Max Holloway', age: 19, unicorns: 3, tel: 325121225 },
    { name: 'Jon Jones', age: 22, unicorns: 1, tel: 326759725 },
    { name: 'Daniel Cormier', age: 21, unicorns: 1, tel: 321119725 },
    { name: 'Brock Lesnar', age: 5, unicorns: 3, tel: 325685991 }
  ];
  constructor() { }

  getUsers() {
    return of(this.users);
  }
}

import { Routes, RouterModule } from '@angular/router'
import { NgModule } from '@angular/core';
import { HoussemComponent } from './houssem/houssem.component';
import { DamyComponent } from './damy/damy.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
    { path: 'houssem', component: HoussemComponent },
    { path: 'damy', component: DamyComponent },
    { path: '', component: HomeComponent }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],

    exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [HoussemComponent, DamyComponent]
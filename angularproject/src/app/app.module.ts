import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule, routingComponents } from './app-routing-module';
import { ColorPickerModule } from 'ngx-color-picker';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { NavigationComponent } from './navigation/navigation.component';
import { UserLineComponent } from './user-line/user-line.component';


@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    HomeComponent,
    NavigationComponent,
    UserLineComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ColorPickerModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
